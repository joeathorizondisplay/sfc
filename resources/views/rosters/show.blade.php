@extends('layouts.master')

@section('content')


<h1>{{ $type->name }} Roster List </h1>
<p class="lead">Here's a list of all {{ $type->name }} players. <a href="/rosters/create">Add a new one?</a></p>
<hr>

<ul class="nav nav-tabs">
	 <li class="active"><a href="/rosters/{{ $type->id }}">All</a></li>
	@foreach($levels as $level)
  <li><a href="/rosters/filter/{{ $type->id }}/{{  $level['id']}}">{{ $level['name']}}</a></li>
      @endforeach
</ul>


<br>

                        <div class="panel panel-primary">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead  style="background-color:#337AB7; color:white">
                                        <tr>  
                                            <th>  </th>
                                            <th>Jersey</th>                                      
                                            <th>Name</th>
                                            <th>Position</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	@foreach($rosters as $roster)
                                        <tr>
                                        	<td><img src="{{ $roster->photo }}" alt="{ $roster->first_name }}&nbsp{{ $roster->last_name}}"  height="42"></td>
                                            <td class="jersey">{{ $roster->jersey }}</td>
                                            <td class="first_name">{{ $roster->first_name }}&nbsp{{ $roster->last_name}}</td>
                                            <td class="position">{{ $roster->position}}</td>
                                            <td> <button type="button" id="use-address" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">Edit</button></td>
                                            <td> {!! Form::open([    'method' => 'DELETE','route' => ['rosters.destroy', $roster->id]]) !!}{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}{!! Form::close() !!}</td>
                                            <td class="height_feet" style="display: none;"  />{{ $roster->height_feet}}</td>
                                            <td class="height_inches" style="display: none;"  />{{ $roster->height_inches}}</td>
                                            <td class="weight" style="display: none;"  />{{ $roster->weight}}</td>
                                            <td class="hometown" style="display: none;"  />{{ $roster->hometown}}</td>
                                            <td class="verse" style="display: none;"  />{{ $roster->verse}}</td>
                                            <td class="food" style="display: none;"  />{{ $roster->food}}</td>
                                            <td class="years_at_sfc" style="display: none;"  />{{ $roster->years_at_sfc}}</td>
                                            
                                        </tr>
                                        @endforeach
                               

                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>


  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          
          <h4 class="modal-title">Edit Player</h4>
        </div>
        <div class="modal-body">
                  <div class="row">
					 
            <div class="col-md-6">
				        <div class="form-group-sm">
			<div class="col-s-3">
<img id="photo" height="100" src="http://images5.fanpop.com/image/photos/28100000/david-david-hasselhoff-28104576-400-300.jpg">

            </div></div></div>
            <div class="col-md-6">
            <div class="form-group-sm">
				<div class="col-s-3">
     {!! Form::label('title', 'First Name:', ['class' => 'control-label']) !!}
    {!! Form::text('first_name', null, ['class' => 'form-control', 'id'=> 'first_name']) !!}
            </div></div></div>
          </div>
                            <div class="row">
            <div class="col-md-6">
				        <div class="form-group-sm">
			<div class="col-s-3">
    {!! Form::label('title', 'Jersey:', ['class' => 'control-label']) !!}
    {!! Form::text('jersey', null, ['class' => 'form-control', 'id'=> 'jersey']) !!}
            </div></div></div>
            <div class="col-md-6">
            <div class="form-group-sm">
			<div class="col-s-3">
    {!! Form::label('title', 'Position:', ['class' => 'control-label']) !!}
    {!! Form::text('position', null, ['class' => 'form-control', 'id'=> 'position']) !!}
            </div></div></div>
          </div>
                            <div class="row">
            <div class="col-md-6">
				        <div class="form-group-sm">
			<div class="col-s-3">
    {!! Form::label('title', 'Height(Feet):', ['class' => 'control-label']) !!}
    {!! Form::text('first_name', null, ['class' => 'form-control', 'id'=> 'height_feet']) !!}
            </div></div></div>
            <div class="col-md-6">
            <div class="form-group-sm">
			<div class="col-s-3">
    {!! Form::label('title', 'Height(Inches):', ['class' => 'control-label']) !!}
    {!! Form::text('first_name', null, ['class' => 'form-control', 'id'=> 'height_inches']) !!}
            </div></div></div>
          </div>
                            <div class="row">
            <div class="col-md-6">
				        <div class="form-group-sm">
			<div class="col-s-3">
    {!! Form::label('title', 'Weight:', ['class' => 'control-label']) !!}
    {!! Form::text('first_name', null, ['class' => 'form-control', 'id'=> 'weight']) !!}
            </div></div></div>
            <div class="col-md-6">
            <div class="form-group-sm">
			<div class="col-s-3">
    {!! Form::label('title', 'Hometown:', ['class' => 'control-label']) !!}
    {!! Form::text('first_name', null, ['class' => 'form-control', 'id'=> 'hometown']) !!}
            </div></div></div>
          </div>
                            <div class="row">
            <div class="col-md-6">
				        <div class="form-group-sm">
			<div class="col-s-3">
    {!! Form::label('title', 'Favorite Bible Verse:', ['class' => 'control-label']) !!}
    {!! Form::text('first_name', null, ['class' => 'form-control', 'id'=> 'verse']) !!}
            </div></div></div>
            <div class="col-md-6">
            <div class="form-group-sm">
			<div class="col-s-3">
    {!! Form::label('title', 'Favorite Food:', ['class' => 'control-label']) !!}
    {!! Form::text('first_name', null, ['class' => 'form-control', 'id'=> 'food']) !!}
            </div></div></div>
          </div>
                            <div class="row">
            <div class="col-md-6">
				        <div class="form-group-sm">
			<div class="col-s-3">
    {!! Form::label('title', 'Years at SFC:', ['class' => 'control-label']) !!}
    {!! Form::text('first_name', null, ['class' => 'form-control', 'id'=> 'years_at_sfc']) !!}
            </div></div></div>
            <div class="col-md-6">
            <div class="form-group-sm">
			<div class="col-s-3">
				  <br>
{!! Form::submit('Create Player', ['class' => 'btn btn-primary']) !!}&nbsp;
<button style="vertical-align: center;" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div></div></div>
          </div>
        
      
{!! Form::close() !!}
</div>
</div>

        
        
        
        
    
   
  
@stop
