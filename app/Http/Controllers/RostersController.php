<?php

namespace App\Http\Controllers;
use Session;
use App\Roster;
use App\Sport;
use App\Level;
use App\Year;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class RostersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rosters = Roster::all();
        $sports = Sport::lists('name', 'id');
        $levels = Level::all();
        $years = Year::lists('name', 'id');

           return view('rosters.index',compact('sports','levels','years'))->withRosters($rosters);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sports = Sport::lists('name', 'id');
        $levels = Level::lists('name', 'id');
        $years = Year::lists('name', 'id');

        return View('rosters.create', compact('sports','levels','years'));
        
  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $input = $request->all();

    Roster::create($input);

    return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($sport_id)

    {       
       $type = Sport::where('id', $sport_id)->first();



		$rosters = Roster::where('sport_id', '=', $sport_id)->orderBy('jersey','DESC')->get();
        $sports = Sport::lists('name', 'id');
        $levels = Level::all();
        $years = Year::lists('name', 'id');

        return view('rosters.show',compact('sports','levels','years'))->withRosters($rosters)->with('type', $type);
    }

    public function filter($sport_id,$level_id)

    {       
       

        $type = Sport::where('id', $sport_id)->first();
        $lev = Level::where('id', $level_id)->first();

		$rosters = Roster::where('level_id', '=', $level_id)->where('sport_id', '=', $sport_id)->orderBy('jersey','DESC')->get();
        $sports = Sport::lists('name', 'id');
        $levels = Level::all();
        $years = Year::lists('name', 'id');

        return view('rosters.filter',compact('sports','levels','years', 'lev'))->withRosters($rosters)->with('type', $type);;
    }





    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
$rosters = Roster::all();
                    $sports = Sport::lists('name', 'id');
        $levels = Level::lists('name', 'id');
        $years = Year::lists('name', 'id');

           return view('rosters.index',compact('sports','levels','years'))->withRosters($rosters);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $roster= Roster::findOrFail($id);

    $roster->delete();

    Session::flash('flash_message_s', 'Player successfully deleted!');
    

     return redirect()->back();
    }
}
